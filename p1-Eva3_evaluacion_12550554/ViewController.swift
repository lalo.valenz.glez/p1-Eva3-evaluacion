//
//  ViewController.swift
//  p1-Eva3_evaluacion_12550554
//
//  Created by TEMPORAL2 on 09/12/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var usuario = ["lalo","majo","alan", "javi"]
    var passwords = ["lalo1234","majo1234","alan1234","javi1234"]

    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    @IBOutlet weak var txtMostrar: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        guardarDatos()
        leerTxt()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func validacion(sender: AnyObject) {
        let urlFileUsuarios = leerUbicacionUsuarios()
        let urlFilePass = leerUbicacionPasswords()
        //verificamos que el archivo exista
        if NSFileManager.defaultManager().fileExistsAtPath(urlFileUsuarios.path!) && NSFileManager.defaultManager().fileExistsAtPath(urlFilePass.path!) {
            //si existe, leer el contenido
            if let usrs = NSArray(contentsOfURL: urlFileUsuarios) as? [String] {
                if let pss = NSArray(contentsOfURL: urlFilePass) as? [String] {
                    for var i = 0; i < usrs.count; i++ {
                        if txtUsuario.text == usrs[i] && txtPass.text == pss[i]{
                            let acceso = txtUsuario.text! + "-" + txtPass.text! + "-acceso correcto\n"
                            txtMostrar.text.appendContentsOf(acceso)
                            let alerta = UIAlertController(title: "Yeah", message: "acceso ocrrecto", preferredStyle: .Alert)
                            let action = UIAlertAction(title: "ok", style: .Destructive, handler: nil)
                            alerta.addAction(action)
                            presentViewController(alerta, animated: true, completion: nil)
                            guardarTxt()
                            break
                        }else{
                            let alerta = UIAlertController(title: "Ups", message: "acceso inocrrecto", preferredStyle: .Alert)
                            let action = UIAlertAction(title: "ok", style: .Destructive, handler: nil)
                            alerta.addAction(action)
                            presentViewController(alerta, animated: true, completion: nil)
                            let acceso = txtUsuario.text! + "-" + txtPass.text! + "-acceso incorrecto\n"
                            txtMostrar.text.appendContentsOf(acceso)
                            guardarTxt()
                            continue
                        }
                    }
                }
            }
        }
    }
    
    func leerTxt(){
        let pathTemp = NSTemporaryDirectory()
        let urlTemp = NSURL(fileURLWithPath: pathTemp)
        let fileTemp = urlTemp.URLByAppendingPathComponent("mitexto.txt")
        do{
            txtMostrar.text = try String(contentsOfFile: fileTemp.path!)
            
        } catch _ {
            let alerta = UIAlertController(title: "Ups", message: "algo anda mal con la lectura", preferredStyle: .Alert)
            let action = UIAlertAction(title: "ok", style: .Destructive, handler: nil)
            alerta.addAction(action)
            presentViewController(alerta, animated: true, completion: nil)
        }
        
    }
    
    func guardarTxt(){
        let pathTemp = NSTemporaryDirectory()
        let urlTemp = NSURL(fileURLWithPath: pathTemp)
        let miArchivo = urlTemp.URLByAppendingPathComponent("mitexto.txt")
        do{
            try txtMostrar.text.writeToURL(miArchivo, atomically: true, encoding: NSUTF8StringEncoding)//String.Encoding
        } catch _ {
            let alerta = UIAlertController(title: "Ups", message: "algo anda mal con la escritura", preferredStyle: .Alert)
            let action = UIAlertAction(title: "ok", style: .Destructive, handler: nil)
            alerta.addAction(action)
            presentViewController(alerta, animated: true, completion: nil)
        }
    }
    
    func leerUbicacionUsuarios() -> NSURL {
        //recupera la ruta de los documentos dentro del dominio como app del usuario y a esa ruta se le pega el nombre del archivo
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls.first!.URLByAppendingPathComponent("usuarios.plist")
        
    }
    func leerUbicacionPasswords() -> NSURL {
        //recupera la ruta de los documentos dentro del dominio como app del usuario y a esa ruta se le pega el nombre del archivo
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls.first!.URLByAppendingPathComponent("passwords.plist")
        
    }
    func guardarDatos () {
        let urlFileUsuarios = leerUbicacionUsuarios()
        let urlFilePasswords = leerUbicacionPasswords()
        //leemos el arreglo de cuatros de texto y lo guardamos
        let arregloUsuarios = (usuario as NSArray)
        arregloUsuarios.writeToURL(urlFileUsuarios, atomically: true)
        let arregloPass = (passwords as NSArray)
        arregloPass.writeToURL(urlFilePasswords, atomically: true)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

